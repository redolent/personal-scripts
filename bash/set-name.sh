#!/bin/bash
error=0


if [[ $_ == $0 ]]; then
  echo "error: script must be source'd"
  error=1
fi

TITLE="$@"



if [ "$TITLE" == "" ] && [ "$error" == "0" ]; then
  #echo "error: please specify a name: set-name.sh <name> "
  #error=1
  echo -n "set title to: "
  TITLE=$( head -n 1 | xargs echo -n )
fi
if [ "$TITLE" == "" ]; then
  echo "error: please specify a name: set-name.sh <name> "
  error=1
fi


if [ "$error" == "0" ]; then

  export PROMPT_COMMAND='echo -ne "\033]0;$( \
          echo "$TITLE"
         )\007"'

  echo "  $TITLE"

fi

