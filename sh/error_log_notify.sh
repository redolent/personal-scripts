#!/bin/bash


set -m   #  job control
set -e


tmp=/tmp/error_log_notify/
mkdir -p $tmp

echo  "using temp file: $tmp/last-tail.txt"


trap 'jobs -p | xargs kill' EXIT


while true
do

    #(
      echo  >  $tmp/last-tail.txt
      sleep 0.5  \
        &&  echo  "Waiting..."  \
        && tail -n 0  -f  /Applications/XAMPP/logs/php_error_log  \
         | tee -a $tmp/last-tail.txt &
      
      #inotifywait -e close_write  $tmp/last-tail.txt 
      tail -n 1 -f  $tmp/last-tail.txt | head -n 1
      #tail -n 0  -f  $tmp/last-tail.txt  |  read -n 20 # wait for file to be changed
      #fswatch --verbose --one-per-batch --one-event  $tmp/last-tail.txt
      echo  "Done waiting."
      sleep 0.5
      set +e
      kill  -s SIGQUIT  %1  # 2>&1  >> /dev/null
      set -e
      
    #) 2>&1 >> /dev/null
    
    
    echo "Notifying..."
    # jobs
    if [ -s "$tmp/last-tail.txt" ]; then
        
        output="$( sed -E 's/^\[.*? ([0-9:]{6,}) .*?\]/\1/g' "$tmp/last-tail.txt" )"
        echo "$output"  |  growlnotify -d "error_log_$RANDOM"
        echo "$output"
    fi
    #cat $tmp/last-tail.txt
    # if 
    # tail -n 0 /Applications/XAMPP/logs/php_error_log > $tmp/last-tail.txt &
  

done  # 2>&1  >> /dev/null

