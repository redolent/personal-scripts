#!/bin/bash


input="$( cat | grep -v "PID" )"

pids=$( awk '{ print $1 }' <<< "$input" ); #echo "$pids"
names="$( grep -oE '[^/]+\.app.*' <<< "$input" | grep -oE '^[^/]+\.app' |  sort | uniq -c )"
echo "Killing" $names "("$(wc -l <<< "$pids" ) "processes ) ..."

# -s SIGINT
cmd="kill  $1  $pids"
echo $cmd
$cmd

