#!/bin/bash
export PATH=$PATH:/usr/local/bin

source=~/work/meta/blinkstick


status=$( cat ~/led-kjkn33VU-1.txt )
altstatus=$( cat ~/led-kjkn33VU-2.txt )


if [ "$status" == "working" ]; then

  node $source/red.js
  
elif [ "$status" == "recently-working" ]; then

  node $source/recent.js
  
elif [ "$status" == "off" ]; then

  # node $source/off.js
  node $source/not-working.js

fi


#if [  "$status" != "off" -a "$altstatus" == "idle" ]; then

#if [ "$status" == "recently-working" ]; then
#  node $source/NOT-blue.js
#el
#if [  "$altstatus" == "idle" ]; then
if [  "$status" == "off" -a "$altstatus" == "idle" ]; then
  node $source/OR-blue.js
else
  node $source/NOT-blue.js
fi

