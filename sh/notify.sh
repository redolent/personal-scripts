#!/bin/bash

if [ "$1" == "" ]; then
    echo "wut"
    exit 1
fi
label="$2"
if [ "$label" == "" ]; then
  label="notify_sh_$RANDOM"
fi


echo "$1" | /usr/local/bin/growlnotify -d "$label"
