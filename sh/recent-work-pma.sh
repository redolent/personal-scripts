ssh pma.nerdydragon.com find dmti/ -size -100k -type f -mtime -0.005 -exec ls -ldh "{}" \\\; \
| grep  -v  '\.swp' | grep -v '/obj/'
