#!/bin/bash

cd /Users/allan/work

#find -size -100k -type f -ctime -0.02


IFS=$'\n'

FILES=$(
  mdfind -onlyin $HOME '((kMDItemContentModificationDate > $time.now(-60m)) && (kMDItemContentModificationDate < $time.now()))'  \
  |  grep    '/Users/allan/Documents/'  \
  |  grep -v 'GoogleDrive'   \
  |  grep -v '/status'       \
  |  grep -v '/recent-work'  \
  |  grep -v '/Dropbox/'     \
  |  grep -v '/lfn/'         \
#  | grep -v '/Library/'     \
#  | grep -v '/Users/allan/Documents/BBEdit Backups'
)

for f in $FILES
do

  #find  "$f"  -maxdepth 0 -size -100k -type f  -mtime -0.005   -exec ls -ldh "{}" \;
  find   "$f"  -maxdepth 0 -size -100k -type f  -mtime -0.0024  -exec ls -ldh "{}" \;
  #ls -ldh "$f"
  
done \
| sort \
| uniq

