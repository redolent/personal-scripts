#!/bin/bash

command="$1"
seconds="$2"


if [ "$seconds" == "" ]; then
  seconds=1
fi

do_clear=false
if [ "$3" == "--clear" ]; then
  do_clear=true
fi

if [ "$3" != "" ]; then
  echo "your doing it wrong"  1>&2 
  exit 1
fi


echo
echo "$command"
while true
do

  if $do_clear; then clear; fi
  
  bash -c "$command"
  sleep "$seconds"

done

