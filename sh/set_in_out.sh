#!/bin/bash


if   [ "$1" == "in" ]; then
	if [ "`osascript ~/Documents/system/Skype/check-skype.scpt`" == "1" ]; then
	    osascript ~/Documents/system/Skype/set-here.scpt > /dev/null &
	fi
elif [ "$1" == "out" ]; then
	if [ "`osascript ~/Documents/system/Skype/check-skype.scpt`" == "1" ]; then
		osascript ~/Documents/system/Skype/set-away.scpt > /dev/null &
	fi
fi
