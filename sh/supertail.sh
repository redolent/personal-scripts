#!/bin/bash

file="$1"

if [ "$file" == "" ]; then
  file="/dev/stdin"
fi

while true;
do
  echo $(  tail -n 1 "$file"  )
  sleep 0.7

done | uniq

