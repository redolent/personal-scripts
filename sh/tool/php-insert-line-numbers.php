<?php


$FILE1 = @$argv[1]; // '@' supresses error messages
$FILE2 = @$argv[2];
$YES   = @$argv[3];

if ( $YES !== "-y" ){

    echo "Usage: ".$argv[0]." <in-file> <file containing a string> -y";
    die;

}


$inbetween_template
           = trim(file_get_contents($FILE2));
$lines     = file($FILE1); // file() returns an array
$newLines  = array();

echo "in between each line: ''$inbetween''\n";

$number = 1;
$newLines[] = $lines[0];
foreach ( $lines as $i => $line ) {

      preg_match('/^(\s*).+$/', $line, $matches ); // magic regex here
      $indent   = @$matches[1];
      $nextline = isset($lines[$i+1])? $lines[$i+1] : "";
      $inbetween= preg_replace("/##*/", strval($number), $inbetween_template);
      $y        =  1 &&  ! preg_match('/^[^{]*els/', $nextline); // next line doesn't start with 'else'
      $y        = $y &&  strpos( $line, ";" ) !== FALSE;         // this line must contain a semi-colon


      $inbetween= sprintf("%-11s", $inbetween);


      if ( $y )
           $newLines[] = $inbetween;
      else $newLines[] = "";
      
      $l = "  ";
      if ( !$y )
           $l = "            ";
      $l .= preg_replace('/\\n$/', "",             $nextline     );
      $newLines[] = $l;

      $newLines[] = "\n";
      
      $number += 1;

}
file_put_contents( "$FILE1.new", implode("", $newLines ));

echo "$FILE1.new - wrote out ". count($newLines)." lines\n";

