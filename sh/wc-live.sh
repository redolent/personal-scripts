#!/bin/bash

count="0"
while IFS='' read -r line ; do
    count=$(( $count + 1 ))
    echo -n -e "\r $count lines "
done
echo

