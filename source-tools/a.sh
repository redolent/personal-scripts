toolsdirectory="$( dirname "$( realpath "${BASH_SOURCE[0]}" )" )"

temp="$(    mktemp  )"
last="$(    mktemp  )"



auto_mode=false
if [ "$1" == "--auto" ]; then
     auto_mode=true
     shift
fi
debug_mode=false
if [ "$1" == "--debug" ]; then
     debug_mode=true
     shift
fi



clipboard_find="$(

    pbpaste -pboard find -Prefer txt

    )"





awhatever(){

    set +e

    # filename match
    if [[ "$1" == *.* ]]; then
       $debug_mode && echo "# filename match"
       $debug_mode && echo "# aafind --... ""$@"" | grep -v ..."
       ~/aafind.sh -o "$@" #| grep -v node_modules
       error_code="$?"
       if [ "$error_code" != "42" ]; then
         exit
       fi
    fi


    #echo  "#  lgrep \"$1\""  1>&2
    if $auto_mode; then
      ~/a-filter-recent.sh --open-result "$@" #| grep -vE '(node_modules|vendor)'
      error_code="$?"
    else
      #$debug_mode && echo "# aaagrep --... ""$@"
      #~/aaagrep.sh -l --all-matching -s "$@"  #| grep  -vE '(node_modules|vendor)'
      $debug_mode && echo "# aaagrep --all-matching --open ""$@"
      ~/aaagrep.sh                   --all-matching --open "$@"
      error_code="$?"
    fi
    
    
    if [ "$error_code" == "42" ]; then
    
          $debug_mode && echo "# aafind --... ""$@"" | grep -v ..."
          echo  "# searching by filename"  1>&2
          # nothing found
          #echo  "#  lfind \"$1\""  1>&2
          ~/aafind.sh -o "$@" #| grep -v node_modules
          error_code="$?"
    
    fi

    if [ "$error_code" != "42" ]; then
    
        $debug_mode && echo "# writing to log at ~/agrep.log"
        echo $(date) a"       ""$1"  >> ~/agrep.log

    fi
}





( ##try
set -e
if [ "$2" == "and" ]; then

    $debug_mode && echo "# a and search"
    echo -n "$@" > "$last"
    "$toolsdirectory"/aunion.sh  --from-a  "$1" "$3"

elif [ "x$1" != "x" ]; then

    echo -n "$@" > "$last"
    awhatever "$@"

elif [ "x$clipboard_find" != "x" ]; then

    $debug_mode && echo "# a clipboard search"
    echo  "# a \"$clipboard_find\""  1>&2
    echo -n     "$clipboard_find" > "$last"
    awhatever "$clipboard_find"

else

    echo "# Nothing to do"  1>&2
    exit 1

fi
)
( ##catch
if [ "$?" != "0" ]; then

    echo  "# ERROR: invalid search string: \"$(cat $last)\""  1>&2
    $debug_mode && echo "# exit 42"
    exit  42

fi
)




# a Quiz.js:29
#  -s should be last flag, so that I can search for ".loadingScreen a"
#  a flag for "auto" mode for when I use a keyboard shortcut
# afind = find *
# a ".resourcesSection p" 
# support new line in place of whitespace (e.g., multiline \s)
