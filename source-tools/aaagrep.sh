#!/bin/bash

# TODO --no-comments
# TODO handle spaces

thisdir="$( dirname "${BASH_SOURCE[0]}" )"
IFS_original="$IFS"
temp="$( mktemp )"

shopt -s expand_aliases


debug_mode=false
if [ "$1" == "--debug" ]; then
     debug_mode=true
     # don't shift yet
fi
thisgrep=grep
if test -e /usr/local/bin/ggrep;
then
     $debug_mode && echo "# found /usr/local/bin/ggrep"
     thisgrep="/usr/local/bin/ggrep"
fi
if test -e /usr/local/bin/gfind;
then
     $debug_mode && echo "# found /usr/local/bin/gfind"
     alias find="/usr/local/bin/gfind"
fi




if [ "$1" == "--debug" ]; then
     thisgrep="$( echo ~/Documents/system/Git/source-tools/fakegrep.sh )"
     shift
fi


alias thisgrep="$thisgrep"


args=()
path=()
first_term=true
nofiles=""
ignorecase=""
filter_by_css=""
filter_by_js=""
special_filter="$" # regex
matching_flag="-m 1" # to not flood output with stuff
do_open=false
for arg in "$@"; do

    if [ "$arg" == "--open" ]; then
          do_open=true
          continue;
          fi
    if [ "$arg" == "--all-matching" ]; then
          matching_flag=""
          continue;
          fi
    if [ "$arg" == "css" ]; then
          filter_by_css="y"
          special_filter="\.css"
          continue;
          fi
    if [ "$arg" == "js" ]; then
          filter_by_css="js"
          special_filter="\.js"
          continue;
          fi

	## filter out arguments,and single letters
	filtered=`echo "$arg" | thisgrep -oE -- '( |^)(-[^ ]*( [0-9]+)?|[a-zA-Z]$)'`
	## change single letters to a dash and a letter
	filtered="$( echo "$filtered" | sed 's/^[^-]$/-\0/g' )"
	if [ "$filtered" = "-s" ]; then
		## treat all the filenames like a whole term string
		nofiles="y"
	fi
	if [ "$filtered" = "-i" ]; then
		## for filtering at the end
		ignorecase="-i"
	fi
	if [ -n "$filtered" ]; then
		args+=("$filtered")
		continue;
	fi

	filtered=`echo "$arg" | thisgrep -oE '( |^)[^- ][^ ]*'`
	if [ -n "$filtered" -a "$first_term" != false ]; then
		term="$arg"
		first_term=false
	else
		path+=("$filtered")
	fi

done
if [ "$nofiles" = "y" ]; then
       term="$term ${path[@]}"
       term="$( sed -E 's/(^ +| +$)//g'<<< "$term" )" # trim
       path=()
fi
#echo "args:" "${args[@]}"
#echo "path:" "${path[@]}"
#echo "term:" "$term"
#exit


$debug_mode && echo "# arguments done"



#  shortcut ".." means any number of characters!
term="$( sed -E 's/(\.)?(\.\.)(\.)?/\3.*\1/g' <<< "$term" )"
#  replace "." to "\."
term="$( sed -E 's/(^|[^\])\.([^*]|$)/\1\\\.\2/g' <<< "$term" )"



# TODO  merge aagrep and this - detect empty path and provide a default
# TODO  get $xx -- the search term and used sed to pull it out

# TODO
#for i in "$@"; do
#    if [[ $i =~ $whitespace ]]
#    then
#        i=\"$i\"
#    fi
#    echo "$i"
# done


#term=`echo "$term" | sed -E s%"(['\"\\])"%'\\\\\1'%g`
# params=`echo $@ | sed -E s%"(['\"\\])"%'\\\\\1'%g`
# echo $params


# find * | thisgrep -o '\.[^/.]*$' |sort | uniq -c | sort -n
#x  39 .tmpl
#0  48 .eot
#0  48 .woff
#0  57 .DS_Store
#0  68 .z
#x  71 .phtml
#0  75 .ttf
#x 101 .txt
#x 114 .less
#x 131 .md
#0 141 .gif
#0 160 .json
#0 251 .jpg
#x 467 .css
#x 485 .xml
#x 803 .html
#0 1851 .png
#0 2371 .map
#x 4458 .php
#x 6024 .js
#  .tmpl
#  .txt
#  .md
#  .phtml
#  .gitignore
#  .coffee
#  .inc

#echo "${path[@]}"; exit;


# cat fixes output issues
# ack --sort-files --show-types -f ${path[@]} \
# No thisgrep -s
#echo otherPath:"'"$otherPath"'";echo "${path}"=="" -a "${path[0]}"==""
if [ "${path}" == "" -a "${path[0]}" == "" ]; then
	otherPath+=*
fi
#echo find "${path[@]}" "$otherPath";exit
#
# for extensions:
#   find * | thisgrep -iEo '\.[^\.\/]+$' | sort | uniq | xargs -L 1 -J % echo -o -type f -name \"* % *\""       \\"
#
$debug_mode && echo "# search beginning"

#echo find "${path[@]}"  $otherPath
#    -L symlinks  https://bit.ly/2EqdNhJ
find -L "${path[@]}"  $otherPath  \
\
   -type f -iname "*.LGPL*"       \
-o -type f -iname "*.TXT*"       \
-o -type f -iname "*.afm*"       \
-o -type f -iname "*.authz*"       \
-o -type f -iname "*.babysitter*"       \
-o -type f -iname "*.bat*"       \
-o -type f -iname "*.coffee*"       \
-o -type f -iname "*.conf*"       \
-o -type f -iname "*.creq*"       \
-o -type f -iname "*.crt*"       \
-o -type f -iname "*.css*"       \
-o -type f -iname "*.scss*"      \
-o -type f -iname "*.less*"      \
-o -type f -iname "*.csv*"       \
-o -type f -iname "*.dist*"       \
-o -type f -iname "*.dll*"       \
-o -type f -iname "*.docx*"       \
-o -type f -iname "*.dphpd*"       \
-o -type f -iname "*.editorconfig*"       \
-o -type f -iname "*.eot*"       \
-o -type f -iname "*.exe*"       \
-o -type f -iname "*.fdf*"       \
-o -type f -iname "*.fla*"       \
-o -type f -iname "*.flf*"       \
-o -type f -iname "*.gif*"       \
-o -type f -iname "*.git*"       \
-o -type f -iname "*.gitattributes*"       \
-o -type f -iname "*.gitignore*"       \
-o -type f -iname "*.gitmodules*"       \
-o -type f -iname "*.gradle*"       \
-o -type f -iname "*.gv*"       \
-o -type f -iname "*.gz*"       \
-o -type f -iname "*.haml*"       \
-o -type f -iname "*.htaccess*"       \
-o -type f -iname "*.htaccess-drupal*"       \
-o -type f -iname "*.html*"       \
-o -type f -iname "*.inc*"       \
-o -type f -iname "*.ini*"       \
-o -type f -iname "*.jar*"       \
-o -type f -iname "*.jpeg*"       \
-o -type f -iname "*.jpg*"       \
-o -type f -iname "*.js*"       \
-o -type f -iname "*.module*"       \
-o -type f -iname "*.ts*"       \
-o -type f -iname "*.json*"       \
-o -type f -iname "*.jshintrc*"       \
-o -type f -iname "*.json*"       \
-o -type f -iname "*.less*"       \
-o -type f -iname "*.map*"       \
-o -type f -iname "*.md*"       \
-o -type f -iname "*.npmignore*"       \
-o -type f -iname "*.opts*"       \
-o -type f -iname "*.out*"       \
-o -type f -iname "*.pem*"       \
-o -type f -iname "*.php*"       \
-o -type f -iname "*.phps*"       \
-o -type f -iname "*.phtml*"       \
-o -type f -iname "*.pl*"       \
-o -type f -iname "*.png*"       \
-o -type f -iname "*.properties*"       \
-o -type f -iname "*.py*"       \
-o -type f -iname "*.refine*"       \
-o -type f -iname "*.req*"       \
-o -type f -iname "*.sass*"       \
-o -type f -iname "*.ser*"       \
-o -type f -iname "*.sh*"       \
-o -type f -iname "*.sql*"       \
-o -type f -iname "*.sreq*"       \
-o -type f -iname "*.sts*"       \
-o -type f -iname "*.tagconfig*"       \
-o -type f -iname "*.template*"       \
-o -type f -iname "*.text*"       \
-o -type f -iname "*.tmpl*"       \
-o -type f -iname "*.tpl*"       \
-o -type f -iname "*.transit*"       \
-o -type f -iname "*.travis*"       \
-o -type f -iname "*.txt*"       \
-o -type f -iname "*.wreqr*"       \
-o -type f -iname "*.wsdl*"       \
-o -type f -iname "*.xml*"       \
-o -type f -iname "*.xsd*"       \
-o -type f -iname "*.yml*"       \
-o -type f -iname "*.z*"       \
\
\
-o -type f -iname "*.ini*"       \
-o -type f -iname "*.java*"      \
-o -type f -iname "*.php*"       \
-o -type f -iname "*.rs*"        \
-o -type f -iname "*.tmpl*"      \
-o -type f -iname "*.txt*"       \
-o -type f -iname "*.md*"        \
-o -type f -iname "*.phtml*"     \
-o -type f -iname "*.html*"      \
-o -type f -iname "*.xml*"       \
-o -type f -iname "*.gitignore*" \
-o -type f -iname "*.coffee*"    \
-o -type f -iname "*.inc*"       \
-o -type f -iname "*.css*"       \
-o -type f -iname "*.less*"      \
-o -type f -iname "*.js*"        \
-o -type f -iname "*.sql*"       \
-o -type f -iname "*.sh*"        \
\
-o -type f -iname "*.cpp*"       \
-o -type f -iname "*.h*"         \
-o -type f -iname "*.pro*"       \
-o -type f -iname "*.qml*"       \
-o -type f -iname "*.qmlc*"      \
-o -type f -iname "*.qrc*"       \
-o -type f -iname "*.rc*"        \
-o -type f -iname "*.ui*"        \
-o -type f -regextype egrep -regex "(.*/)?[^.]*$"  \
 2> /dev/null \
\
  | thisgrep -vE '(/tags$|www/|node_modules|vendor)'  \
  | thisgrep -viE '(/cache/|--[a-z-]*$|[-]$|backup[0-9.~-]*$)'  --line-buffered   \
  | perl -ne '$H{$_}++ or print' \
  | parallel -j200% -n 1000 -m "$thisgrep" -n -H --color=never --line-buffered "\"$term\"" "${args[@]}"  {} \
  | sed -E 's/(.{200,200}).*$/\1[...]/'  \
\
  | thisgrep --color=auto -E "($term|$)"  \
  | thisgrep $special_filter  \
  > "$temp"

$debug_mode && echo "# search done"

#  | tee "$temp"  \
  
#  | parallel -j200% -n 1000 -m 'sleep 2 && echo -e ZZ "{}"\\n\\n\\n'
  
#  \
#  | parallel -j200% -n 1000 -m thisgrep -H --color=auto --line-buffered "\"$term\"" "${args[@]}" {} \
#  | parallel -j200% -n 1000 -m 'sleep 1 && echo -e ZZ "{}"\\n\\n\\n'
  
  #\
  #| thisgrep --line-buffered -E -i "($term|$)" --color=auto \
  #| (sleep 1 && xargs echo)

#  | tee "$temp" \

#   | parallel -k -j150% -n 1000 -m thisgrep -H --color=always --line-buffered "\"$term\"" "${args[@]}" {} \



term__clean="$( echo -n "$term" | tr -d '\\' )"

output_count=$( wc -l < "$temp" )


# This works as of 2014-09-05
if [[ " ${args[@]}" == *" -l"* ]] && [ $output_count = 1 ]; then
	#
	#  if we are using -l and find one file: open it
	#
	#echo "copied to paste board: find"
	echo -n "$term__clean" | pbcopy -pboard find
	echo $(date) lgrep"  " "$term__clean"  >> ~/agrep.log
	file="$( cat "$temp" )"
	# change token-splitting to avoid issues with spaces in filename
	IFS=$'\n'
	filedetail="$(
	# have the editor open at the specified line
	thisgrep -nH $ignorecase "$term" $file $matching_flag | sed -E 's/(^[^:]+:[0-9]+)?(: )?.*$/\1/'
	)"
	echo    "$filedetail"
	bbedit --front-window "$filedetail"
	
elif [ $output_count -ge 1 ]; then
	$debug_mode && echo "$output_count -ge 1"
	# change token-splitting to avoid issues with spaces in filename
	IFS=$'\n'
	#echo "copied to paste board: find"
	echo -n "$term__clean" | pbcopy -pboard find
	echo $(date) lgrep"  " "$term__clean"  >> ~/agrep.log
	if [[ " ${args[@]}" == *" -l"* ]] && [ $output_count -lt 20 ]; then
	    $debug_mode && echo " ${args[@]} == *\" -l\"*"
	    #
	    #  if we are using -l and there are less than 20 files,
	    #  then show the numbers
	    #
	    IFS=$'\n'
	    files=$( cat "$temp" \
	             |  sed -E 's/((^[^:]+)(:[0-9]+)?)?(: )?.*$/\2/' \
	             |  sort | uniq   )
	    thisgrep  -n -H --color=never --line-buffered  \
	          $matching_flag  \
	          $ignorecase  \
	         "$term"  \
	         $files                \
	         | sed -E 's/(^[^:]+:[0-9]+)?(: )?.*$/\1/'
	         # "$term" "${args[@]}"  \
	else
	    cat "$temp" # echo results
	fi
	if [[ " ${args[@]}" == *" -o"*  ]] || $do_open; then
		$debug_mode && echo " ${args[@]} == *\" -o\"*"
		IFS="$IFS_original"
		#thisgrep -Eo '^[^:]*:[0-9]*' "$temp"
		#exit
		file="$(ls -tr $("$thisgrep" -Eo '^[^:]*' "$temp") | tail -n 1 )"
		#echo "# grep -nHE "$term" "$file" |  head -n 1 | thisgrep -Eo '^[^:]*:[0-9]*' "
		file=$( "$thisgrep" -nHE "$term" "$file" |  head -n 1 | "$thisgrep" -Eo '^[^:]*:[0-9]*' )
		#echo "# "bbedit \"$file\"
		bbedit --front-window "$file"
	fi
	
elif [ $output_count == 0 ]; then
	#
	exit  42
	#
else
	cat "$temp" # echo results
	#echo -n '';
	#thisgrep --line-buffered -E -i "($term|$)" --color "$temp"
fi



