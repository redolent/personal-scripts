#!/bin/bash
open_it=""

if [ "$1" = "-o" -o "$1" = "o" ]; then
   open_it="-o"
   shift;
   fi





term="$1"
shift





#term="$( sed s/\.\.*\./*/g <<< "$term" )"
term="$( sed -E 's/(\.)?(\.\.)(\.)?/\3*\1/g' <<< "$term" )"



if [ "$1" == "--all" ]; then
	shift
	# 2='*'
fi


if [ "$1" = "-o" -o "$1" = "o" ]; then
   open_it="-o"
   shift;
fi



##
##  allows search for dir/file
##
OLD_IFS="$IFS"
IFS=$'\n'
if [[ "$term" == */* ]]; then
	arr=$(echo "$term" | tr "/" "\n")
	term=""
	term_dir=""
	for token in $arr; do
		[ $term ] && term_dir="$term_dir$term[^/]*/"
		term="$token"
	done
fi
IFS="$OLD_IFS"


#echo term_dir="$term_dir"; echo term="$term"; exit


#echo "$1"; exit
if [ "$1" != "" ]; then
	paths=( "$@" )
	manual_mode="true"
else
	if [ ! -d application -o ! -d common ]; then
		#echo "~aafind: warning, you are not in the codebase directory" 1>&2
		#exit 1
		paths=(
			*
			)
	elif [ -d htdocs -a ! -d public ]; then
		paths=( 
			common
			application/
			www
			htdocs
			)
		echo "~aafind: ${paths[@]}" 1>&2
	else
		paths=( 
			common
			application/
			public
			)
		echo "~aafind: ${paths[@]}" 1>&2
	fi
fi
# echo ${paths[@]} 1>&2
# exit 1


filter_results()
{
	# split
	if [ "$term_dir" != "" ]; then
		cp "$temp" "$temp2"
		grep -iE "$term_dir" "$temp2" > "$temp"
	fi

	# get rid of garbage
	cp "$temp" "$temp2"
	grep -vE '(/tags$|www/|node_modules|vendor|node-libraries/.*\.web.js)' "$temp2" > "$temp"
}



# temporary files to store the lists
temp="$(  mktemp )"
temp2="$( mktemp )"


#
#   MAGIC
#
#
for path in "${paths[@]}"; do
	#    -L symlinks  https://bit.ly/2EqdNhJ
	find -L  "$path" -iname "*$term*"  |  uniq  >> "$temp"
done


filter_results;
#
#
#
#


# 
# 
# if [ ! -s "$temp" ]; then
# 	echo "~aafind: ø expanding search (vendor)" 1>&2
# 	for path in "${paths[@]}"; do
# 		find "$path" -iname "*$term*"  |  uniq   >> "$temp"
# 	done
# fi
# 
# 
# filter_results;
# 
# 
# if [ ! -s "$temp" -a "$manual_mode" != "true" ]; then
# 	echo "~aafind: ø searching *" 1>&2
# 	find * -iname "*$term*"  |  uniq  >> "$temp"
# fi
# 
# if [ ! -s "$temp" ]; then
# 	echo "~aafind: ø nothing found for *$term*" 1>&2
# fi
# 
# 
# 
# filter_results;


target_file="$( ls -trd $(cat "$temp") | tail -n 1 )"
term_regex=`echo "$term" | sed "s%\*%[^/]*%g" | sed "s%\?%[^/]%g" `
cat "$temp"  | 
  sed -E 's%//+%/%'  |
  grep  --color=always -E    "$target_file|$term_regex|$"
  #grep --color=always -E -i "$term_regex|$"
  #grep --color=always -E -i "$term_dir$|$term_regex$|$"


#echo "$term_dir$|$term_regex$|$"
#echo '$target_file = '$target_file 
#exit;


# echo a
# cat "$temp"
# echo b
# cat "$temp2"

if [ "$open_it" = "-o"  -a  $( wc -l < "$temp" ) = 1 ]; then
	echo $(date) lfind"  " "$term"  >> ~/agrep.log
	bbedit --front-window `cat "$temp"`
elif [ "$open_it" = "-o"   -a  $( wc -l < "$temp" ) != "0" ]; then
	#ls -trd $(cat "$temp") | tail -n 1 | xargs bbedit --front-window
	#echo "#" bbedit --front-window "$target_file"
	bbedit --front-window "$target_file"
fi


output_count=$( wc -l < "$temp" )
if [ $output_count == 0 ]; then

    exit  42

fi

