#!/bin/bash
output="$(  mktemp  )"
temp="$(    mktemp  )"
if [ "$1" == "--help" ]; then
     echo "# Usage:  afilter  string1 [string2] [string3] ... "    1>&2
     echo "# What:   narrows down search results, opens the file"  1>&2
     exit 1
fi
error(){
     echo "# filter:" "$@"  1>&2
     exit
}



#
#
##  pull the results into the temp file
#   removing empty lines
cat | grep -v '^$'  >  "$output"
if  [ ! -s "$output" ]; then
            error  "no matches; empty set was given"
fi
#
#
##  parameter checks
if [ "$1" == ""  -a  $( wc -l < "$output" ) = 1 ]; then
     ##  the user wants us to open the only result found
     bbedit `cat "$output"`
     exit
fi




#
#
##  filter by multiple terms in each token given
for term  in  "$@"; do
    #
    #
    ##  1. clean the search term
    #      This enables ".." to mean a wildcard, i.e., ".*"
    #      Remember, this is for the search term
    #      not the file names (not stdin)
    #
    term="$( echo "$term" | sed 's/\.\./.*/g' )";
    #
    #
    ##  2. do the filtering
    if [[ "$term" =~ ^[0-9][0-9]?$ ]]; then

          cat "$output" | head -n $[$term+0] |
                          tail -n 1  >  "$temp"

    else
          grep "$term" -i "$output"  >  "$temp"
    fi
    #
    #
    ##  3. return an error, if applicable
    if  [ ! -s "$temp" ]; then
                error  "no matches; for" \
                       "\"$term\" in result set"
    fi
    #
    #
    ##  4. swap variables
    cat "$temp" > "$output"


done
#
#
##  5. show the result set in the terminal
cat "$output"
#
#
##  6. open the file
if [ $( wc -l < "$output" ) = 1 ]; then

        bbedit `cat "$output"`

fi


