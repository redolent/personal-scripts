dir="~"
##
##
##
##
## source tools

    ##  agrep shows the code for matches
    alias    agrep='  $dir/aaagrep.sh'
    alias    ag='     $dir/aaagrep.sh'
    ##  lgrep shows matches and opens the file
    alias    lgrep='  $dir/aaagrep.sh -l'
    alias    lg='     $dir/aaagrep.sh -l'

    ##  afind shows file matches. lfind opens the file
    alias    afind='  $dir/aaafind.sh'
    alias    lfind='  $dir/aaafind.sh -o'
    alias    lf='     $dir/aaafind.sh -o'

    ##  these filters will automatically open the file
    alias  afilter='  $dir/afilter.sh'
    alias  af='       $dir/afilter.sh'
    alias  f='        $dir/afilter.sh'
    alias  js='       $dir/afilter.sh  js'
    alias  css='      $dir/afilter.sh  css'

    ## old stuff
    #alias   aagrep='   $dir/aagrep.sh'
    #alias  aaagrep='  $dir/aaagrep.sh'
    #alias   aafind='   $dir/aafind.sh'
    #alias  aaafind='  $dir/aaafind.sh'
    #alias    Lgrep='  $dir/lfn-grep.sh'
    #alias  agrl=grep -Rl

##
##
##
##
## aliases: git related
    alias g='      git'

    alias ga='     git add'

    alias gd='     git diff   --minimal'
    alias gd.='    git diff   --minimal  .'
    alias gdc='    git diff   --minimal --cached'
    alias gdc.='   git diff   --minimal --cached  .'

    alias gs='     git status   --column'
    alias gs.='    git status   --column . '
    alias gsu='    git status   --column . -uno '

    alias cgt='    clear; git status --column'
    alias cgtu='   clear; git status --column --untracked-files=no'


    alias gpush='   git push origin `git rev-parse --abbrev-ref HEAD`'
    alias gpull='                git pull --rebase origin `git rev-parse --abbrev-ref HEAD`'

    #alias gitls='  $dir/gitls.sh'
    #alias gpstash=' git stash && git pull --rebase origin `git rev-parse --abbrev-ref HEAD` && echo "$?" && git stash pop --rebase'
    #alias gspr='    git stash pop --rebase'
    #alias p="cgtu ."
##
##
##
##
## custom scripts
    #alias setname='   source ~/sh/set-name.sh'
    #alias foo='       slappasswd -g'
##
##
##
##
## aliases: linux related
    alias ls='ls -h'
    alias mv='mv -i'
    alias cp='cp -i'
    alias cd..='   echo "cd .."; cd ..'
    alias cd...='   echo "cd ../.."; cd ../..'
##
##
##
##
## custom shortcuts
    alias b=bbedit
    alias bn='bbedit --new-window'
    alias androidstudio='open -a "Android Studio"'
    alias eclipse='open /Applications/Development/eclipse/Eclipse.app  --args'
    #alias tt=~/time.sh
    #alias jj=~/journal.sh
    #alias 'cd-adx'='~/log.sh cd-adx;  cd -P /Users/allan/Documents/work/appdetex/code/htdocs/..; pwd'
    #alias 'cd-lfn'='~/log.sh cd-lfn;  cd -P /Users/allan/lfn && . ./cd.sh; pwd; git status . -uno'
    #alias 'cd-lavu'='~/log.sh cd-lavu;  cd -P /Users/allan/work/jdd/projects/lavu/lavu.com; pwd; echo; ls; echo -n '
    #alias ss=~/Documents/system/macs/spotted.sh





# alias g='     ~/log.sh g;      git'
# alias ga='    ~/log.sh ga;     git add'
# alias gd='    ~/log.sh gd;     git diff --minimal'
# alias gdc='   ~/log.sh gdc;    git diff --minimal --cached'
# alias cgt='   ~/log.sh cgt;    clear; git status --column'
# alias cgt.='  ~/log.sh cgt;    clear; git status --column .'
# alias cgtu='  ~/log.sh cgt;    clear; git status --column --untracked-files=no'
# alias cgtu.=' ~/log.sh cgt;    clear; git status --column --untracked-files=no .'
# alias gitls=' ~/log.sh gitls;  ~/gitls.sh'
# alias gpush='   ~/log.sh gpush;   git push origin `git rev-parse --abbrev-ref HEAD`'
# alias gpull='   ~/log.sh gpull;                git pull --rebase origin `git rev-parse --abbrev-ref HEAD`'
# alias gpstash=' ~/log.sh gpstash; git stash && git pull --rebase origin `git rev-parse --abbrev-ref HEAD` && echo "$?" && git stash pop --rebase'
# alias gstash='  ~/log.sh gstash;  git stash'
# alias gspr='    ~/log.sh gspr;    git stash pop --rebase'
# alias gsgpgpgsp=' git stash && git pull --rebase origin `git rev-parse --abbrev-ref HEAD` && git stash pop'
# 
# alias fuck=' echo -bash: fuck: command not found. Please fuck'
# 
# 
# 
# #
# #
# #
# #
# #
# alias  ccat='pygmentize -g'
# alias  latest='ls -tr | tail -n 1'
# #
# #
# #
# #
# #
# alias  yoink="open -a yoink "
# 
# 
# 
