shopt -s expand_aliases
toolsdirectory="$( dirname "$( realpath "${BASH_SOURCE[0]}" )" )"
alias lgs="$toolsdirectory/aaagrep.sh -l -s"


we_launched_from_script=false
if [ "$1" == "--from-a" ]; then
      we_launched_from_script=true
      shift
fi


term1="$1"
term2="$2"
log() {
    if ! $we_launched_from_script; then
    
          echo "$@"  1>&2
    fi
}



log "# searching for set A:  $1"
result1="$(lgs $term1 | sort)"



log "# searching for set B:  $2"
result2="$(lgs $term2 | sort)"



comm  -12  <(echo "$result1")  <(echo "$result2")
