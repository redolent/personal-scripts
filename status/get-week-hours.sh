#!/usr/local/bin/node

var now     = new Date();
var sunday  = new Date();

sunday.setDate( sunday.getDate() - now.getDay() );
sunday.setHours( 0 );
sunday.setMinutes( 0 );
sunday.setSeconds( 0 );


console.log( (( now.getTime() - sunday.getTime() ) / 1000 / 60 / 60 ).toFixed(1) )

