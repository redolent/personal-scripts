#!/bin/bash

##
# aliases

  shopt -s expand_aliases
  alias sed="/usr/local/bin/gsed"
  alias grep="/usr/local/bin/ggrep"
  alias uptime="/usr/local/opt/coreutils/libexec/gnubin/uptime"

##
#


DEBUG=0
if [ "$1" == "--debug" ]; then
  DEBUG=1
  shift
fi
if [ "$1" == "--success" ]; then
  echo "HELLO WORLD"
  shift
  exit
fi
NOIP=false
if [ "$1" == "--noip" ]; then
  #echo "--noip"; exit;
  NOIP=true
  shift
fi
if [ "$1" == "--version" ]; then
        grep  --version | head -n 1
        sed   --version | head -n 1
        bc    --version | head -n 1
        bash  --version | head -n 1
        exit
fi
logDisplay()
{
  if [ "$DEBUG" == "1" ]; then
    echo "render: showing module '"$@"'" 1>&2
  fi
}
log()
{
  if [ "$DEBUG" == "1" ]; then
    echo "# $1 $2 $3" 1>&2
  fi
}
log2()
{
  if [ "$DEBUG" == "1" ]; then
    echo "  ##  $1 $2 $3" 1>&2
  fi
}
log3()
{
  if [ "$DEBUG" == "1" ]; then
    echo "      ###  $1 $2 $3" 1>&2
  fi
}
log4()
{
  if [ "$DEBUG" == "1" ]; then
    echo "           ####  $1 $2 $3" 1>&2
  fi
}
noop()
{
  a=0; # do nothing
}




I_AM_SCRIPT="$( 
	echo "$(dirname "${BASH_SOURCE[0]}")"/"$(
		readlink "${BASH_SOURCE[0]}"
		)"
	)"
cd "$( dirname "$I_AM_SCRIPT" )"

#
# constants
#
EN_SPACE=" " #special character
#EN_SPACE=" " #space
#SEPERATOR_DOT=" • " # U+2001 EM QUAD
#SEPERATOR_DOT=" • " # U+2003 EM SPACE
SEPERATOR_DOT="$EN_SPACE•$EN_SPACE"  # U+2002 EN SPACE
SEPERATOR_DOT="$EN_SPACE•$EN_SPACE" 
SEPERATOR_DOT_LEFT="$EN_SPACE•"
#SEPERATOR_DOT="X"
#EN_SPACE="Y"

if [ "$DEBUG" == "1" ]; then
  # override for debugging
  EN_SPACE="-"
  SEPERATOR_DOT="-|-"
fi




clean()
{
	string="$( cat )"
	log=~/wtf.txt
	badchar="[^100%a-z0-9()%?!.,;:-⁺]+"

	#echo -n "$string" | sed -E 's/\W/ /g'
	#echo -n "$string" | sed -E 's/[^a-z0-9%?!.,;:-]+/x/gi'
	#echo -n "$string" \
	#	| tee -a "$log"
	
	# to clean:
	echo -n "$string" | sed -E 's/'"$badchar"'/ /gi'
	# or not to clean
	#echo -n "$string"

	# for debugging
	#echo -n "$string==>$( echo "$string" | sed -E 's/'"$badchar"'/X/gi' )"  >> "$log"
	#echo "" >> "$log"
}
bcx()
{
	#bc -l; return
    bc -l << EOF
    # http://phodd.net/gnu-bc/code/funcs.bc
    
    # Round down to integer below x
	define floor(x) {
	  auto os,xx;os=scale;scale=0
	  xx=x/1;if(xx>x).=xx--
	  scale=os;return(xx)
	}

	# Round up to integer above x
	define ceil(x) {
	  auto os,xx;x=-x;os=scale;scale=0
	  xx=x/1;if(xx>x).=xx--
	  scale=os;return(-xx)
	}
	
	$@
	
EOF
}


if false; then
  #echo "Wha3t?" | clean
  version=$(git log  --oneline "$I_AM_SCRIPT" | wc -l | xargs echo )


  # todo move down
  echo -n "v$version "
  echo  -n "$SEPERATOR_DOT"
fi


#top -n43 -l2 -o-CPU > top.txt && tail -n52 top.txt


#
#  Wifi is first to help with $Calm_network variable
#
Calm_network=false
log "Wifi Info"


    airportinfo="$(
      /System/Library/PrivateFrameworks/Apple80211.\framework/Versions/A/Resources/airport --getinfo
      )"
    ssid=$( grep ' 'SSID <<< "$airportinfo" | sed 's/^.*SSID: //' )
    ssid_raw=$ssid
    if grep --quiet 'AirPort: Off' <<< "$airportinfo"; then
        ssid='[Wi-Fi Off]'
    fi
    if [ "$small_screen" = true ]; then
        # abbreviate ssid with an elipsis
        ssid=$( sed -E 's/([^\s]{1,3}.{2,2}[^\s]{1,3}).+([^\s]{1,3}.{2,2}[^\s]{1,3})/\1...\2/' <<< "$ssid" )
    fi
    ssid="$ssid "
    ssid=$( sed 's/ / /g' <<< "$ssid" ) # special utf8 character U+2005
                                        # (https://www.cs.tut.fi/~jkorpela/chars/spaces.html)
    wired_string=""
    #is_wired=$( ifconfig | grep en4: | wc -l | xargs echo )
    #if [ "$is_wired" -eq "1" ]; then
    #	wired_string="wired"
    #fi


    if [ "$ssid_raw" == "Invisible-Inkless-Printer" ]; then
      #whynot
      ssid="inkless"
    elif [ "$ssid_raw" == "Trailhead" ]; then
      ssid="TH"
    elif [ "$ssid_raw" == "Trailhead-WiFi" ]; then
      ssid="TH"
    elif [ "$ssid_raw" == "springtime 5GHz" ]; then
      ssid=" "
    elif [ "$ssid_raw" == "Imagination" ]; then
      #Calm_network=true
      ssid="slow network"
    elif [ "$ssid_raw" == "CenturyLink1907" ]; then
      Calm_network=false
      ssid=""
    elif [ "$ssid_raw" == "Trailhead-WiFi19" ]; then
      Calm_network=false
      Report_when_offline=true
      ssid=""
    elif [ "$ssid_raw" == "Imagination2" ]; then
      Calm_network=true
      ssid=" "
    elif [ "$ssid_raw" == "NETGEAR89-5G" ]; then
      ssid="5Ghz"
    elif [ "$ssid_raw" == "NETGEAR89" ]; then
      ssid="2.4Ghz"
    elif [ "$ssid_raw" == "BHNTG1672G1152-5G" ]; then
      ssid="blah-net"
    elif [ "$ssid_raw" == "Dawson Taylor Guest 5GHz" -o "$ssid_raw" == "Dawson Taylor Guest 2.4GHz" ]; then
      Calm_network=true
      ssid=""
    elif [ "$ssid_raw" == "Tango && Alina" ]; then
      ssid=""
    fi


if $NOIP; then
      Calm_network=true
      ssid="" # note this is not expected behavior
fi




#
#  Clipboard
#
clipboard="\`$( 
                 echo -n $(pbpaste) \
		 | head -c 60
                 # | head -n 1
                 # | cut -c 1-120 
              )\`"




#
#  Screen size
#
  log "Screen size"
# http://hints.macworld.com/article.php?story=20090208121119440
# https://github.com/jhford/screenresolution
#screen_width=$( xrandr --current 2>&1 | grep -Eo 'current[^,]+' | awk '{print $2}' )
#screen_width=$( defaults read /Library/Preferences/com.apple.windowserver | grep -w Width | head -n 1 | grep -oE "[0-9]+")
screen_width=$( /usr/local/bin/screenresolution get 2>&1 | grep -oE 'Display 0: [0-9]+' | grep -Eo '[0-9]+$' )
screen_width_px=$screen_width
if [ "$screen_width_px" == "2880" ] || [ "$screen_width_px" == "1920" ]; then
	screen_width=""
else
	screen_width2=$( echo "$screen_width" | sed -E 's%(.*)(...)%\1,\2%g' )
	if [ "$screen_width"  == "" ]; then
	   noop
	elif [ "$screen_width2" == "" ]; then
		screen_width="(${screen_width}px)"
	else
		screen_width="(${screen_width2}px)"
	fi
fi

[ "$screen_width_px" -lt "1500" ] && \
	small_screen=true || \
	small_screen=false
[ "$screen_width_px" -lt "2000" ] && \
	medium_screen=true || \
	medium_screen=false
[ "$screen_width_px" -lt "2000" ] && \
	screen_width=""

log2 $screen_width_px




#
#  Work message
#
  log "Work message"

if false; then

work_temp_file="$( ls -tr /Users/allan/Documents/work/invoices/time-logs/20??-??-??.txt | tail -n 1 )"
if false; then
  work_temp="$( grep -v '\[to' "$work_temp_file" | grep -vE '(\(\(|\)\))' | tail -n 1 )"
  work_time=`~/work_time.sh`
  work_msg=$( echo "$work_temp" | cut -d ' ' -f 2- | xargs echo )
  work_msg=$( grep -oE '[^,;:)]*\)?$' <<< "$work_msg" | xargs echo )
  work="\"$work_msg\" "$(( $work_time/60 ))":"$(( $work_time%60/10 ))$(( $work_time%60%10 )) 
else
  work_temp="$( grep -v '\[to' "$work_temp_file" | grep -vE '(\(\(|\)\))' | tail -n 1 )"
  work_msg=$( echo "$work_temp" | cut -d ' ' -f 2- | xargs echo )
  work_msg=$( grep -oE '[^,;:)]*\)?$' <<< "$work_msg" | xargs echo )
  simple_work_msg=$( sed -e 's/|.*$//' <<< "$work_msg" | sed -E 's/(.*) *<> *(.*)/👾 \1 \2/' | sed 's/\s\s*/ /g' )
fi
#echo "X= $work_msg"

fi
work_ZXtEjweK="$( ~/work_time.sh )"
log2 "Work = $work_ZXtEjweK"
if [ "$work_ZXtEjweK" -lt 900 ]; then  # 900 = 15 hours

  work_time="$work_ZXtEjweK"
  work=$(( $work_time/60 ))":"$(( $work_time%60/10 ))$(( $work_time%60%10 )) 
  work_time=""

fi
if  $Calm_network; then
   work="$work time"
fi


#echo "find "$work_temp_file" -mmin -60"; exit
if test -n `find "$work_temp_file" -mmin -60`; then
  # file has been modified recently
  work_msg=""
  simple_work_msg=""
fi


runLinesChanged()
{
		cd -P /Users/allan/Documents/work/appdetex/code/htdocs/..
		$@ \
			| grep -oE '([0-9]+ (insert|dele))' \
			| grep -oE '[0-9]+' \
			| xargs echo \
			| sed 's% % + %g'
};
if false; then
  #linesChanged1=`cd -P /Users/allan/Documents/work/appdetex/code/htdocs/..; git diff --shortstat --minimal          | grep -oE '([0-9]+)'    | tail -n 1 | xargs echo`
  #linesChanged2=`cd -P /Users/allan/Documents/work/appdetex/code/htdocs/..; git diff --shortstat --minimal --cached | grep -oE '([0-9]+|\+)' | tail -n 3 | xargs echo | bc`
  linesChanged1=`runLinesChanged git diff --shortstat --minimal --ignore-submodules`
  linesChanged2=`runLinesChanged git diff --shortstat --minimal --ignore-submodules --cached`
  linesChanged3=`runLinesChanged git submodule foreach git diff --shortstat --minimal`
  linesChanged=`echo $linesChanged1-0 + $linesChanged2-0 + $linesChanged3-0 | bc`
  #🔫🍺🎉👻💉🚬
  # if   [ "$linesChanged" -ge "300" ]; then  linesChanged="$linesChanged 👻 "
  # elif [ "$linesChanged" -ge "150" ]; then  linesChanged="$linesChanged 🔫🍺 "
  # elif [ "$linesChanged" -ge "100" ]; then  linesChanged="$linesChanged 🍺🍺 "
  # elif [ "$linesChanged" -ge  "50" ]; then  linesChanged="$linesChanged 🍺 "
  # elif [ "$linesChanged"  !=   "0" ]; then  linesChanged="$linesChanged"
  if [ "$linesChanged"  !=   "0" ]; then  linesChanged="$linesChanged"
  else
    linesChanged="";
  fi
  if [ "$linesChanged"  != "" ]; then
    # http://www.csbruce.com/software/utf-8.html
    # ☰ ☱ ☲ ☳ ☴ ☵ ☶ ☷
    linesChanged="☱$linesChanged"
  fi
fi





#
#  Power
#
  log "Power"
  log2 "Watt calculation"

  log3 "running system_profiler..."
  
power_all="$( /usr/sbin/system_profiler SPPowerDataType | grep -iE '(amp|volt)' )"
power_volt="$( grep -i volt <<< "$power_all" | grep -Eo '[0-9.-]+' )"; #echo $power_volt 
power_amp="$(  grep -i amp  <<< "$power_all" | grep -Eo '[0-9.-]+' )"; #echo $power_amp
#echo -n "|_"$power_all"_| "

  log3 "done running system_profiler"

log3 "math for watts"
power_watt=$( 
	bc -l <<< "$power_volt*$power_amp / 1000000" \
	| grep -oE "^[0-9.+-]{0,5}" \
	| sed -E 's/(^|[^0-9]+)\./\10./' \
	); #echo $power_watt; exit

log3 "math for percent"
pmset_batt="$( pmset -g batt )"
batt_remaining=$(  echo "$pmset_batt" | grep -oE '[^ ]+ remaining' | sed 's/remaining//g' | xargs echo )
batt_percentage=$( echo "$pmset_batt" | grep -oE '[0-9.]+%' )
batt_percentage_raw=$( tr -d ' %' <<< "$batt_percentage" )
#echo $batt_percentage_raw; echo $batt_percentage; exit



#
# I square this because I need an absolute value
#
WATTSUFFIX="W"
MILLIWATTSUFFIX="mW"
if  $Calm_network; then
  WATTSUFFIX=" watt"
  MILLIWATTSUFFIX=" milliwatt"
fi
if $small_screen; then
  WATTSUFFIX="W"
  MILLIWATTSUFFIX="mW"
fi

if [ "$( bc -l <<< "${power_watt}^2 < 1" )" = "1" ]; then
	power_watt_W="$( bc -l <<< "${power_watt}*1000" | sed -E "s/\.[0-9]*$//g" )"$MILLIWATTSUFFIX 
else
	power_watt_W="${power_watt}$WATTSUFFIX"
fi


log3 "power=\"${power_watt_W}\"";
power="${power_watt_W}";
Is_discharging=false
if [ "$power_amp" == "0" ]; then
	noop
elif [ "$power_amp" -lt "0" ]; then
	#power_watt="⁻$( bc -l <<< -"($power_watt)" )"
	# power_amp="⁻$( bc -l <<< -"($power_amp)"  )"
	Is_discharging=true
	noop
	#power="⁻${power_watt_W}";
else
	power_amp_raw="$power_amp"
	power_watt="⁺$power_watt_W"
	power_amp="⁺$power_amp"
	power="⁺${power_watt_W}";
fi
#if [ "$small_screen" = false ]; then
#	power="${power_amp}mA ${power_watt_W}"; # echo $power; exit;
#else
#	power="${power_watt_W}";
#fi
if [ "$power_amp" == "0" ]; then
	power=""
fi
#power="POWER $power"
power=" $power"


log2 "Battery"

# uncomment this
#
#  Hey note this part below
#
SUFFIX="h"
if  $Calm_network && $Is_discharging; then
  SUFFIX=" bat"
fi

###mytty=`ps ax | grep x$$ | awk '{ print $2 }'`
###processes=`ps -A | wc -l | xargs echo`
###processes=`echo $psListed | grep -v '[0-9]* '$mytty | wc -l | xargs echo '-1 + ' | bc`
#psListed=`ps -A`
#processes=`echo "$psListed"-0 | wc -l | xargs echo '-2 + ' | bc`
##cycles=`ioreg -n AppleSmartBattery | grep '"CycleCount"' | grep -oE '[0-9]+'`
##capacity=$( ioreg -l | grep -i capacity ) # http://hints.macworld.com/article.php?story=20100130123935998
## date -d "1970-1-1 0:00 +255.55 seconds" "+%M:%S"
log3 "batt_time_at_full_charge = ($batt_remaining) / ($batt_percentage)"
  batt_time_at_full_charge_formula=$( echo "($batt_remaining) / ($batt_percentage)" \
      | sed 's%:%+(1/60)*%g' | sed 's%\%%/(100)%g'
      )
log3 "batt_time_at_full_charge_formula = ($batt_time_at_full_charge_formula)"
  batt_time_at_full_charge=$(
      echo "$batt_time_at_full_charge_formula" | bc -l \
      | sed 's%\.%:%g' \
      | grep -oE '[0-9]*[:.][0-9]{0,2}' 
      )
log3 "batt_remaining = $batt_remaining"
log3 "batt_time_at_full_charge = ($batt_time_at_full_charge)"
log3 "logic"
  #echo $batt_time_at_full_charge; exit

  if [ "$batt_remaining" == "0:00" -o "$batt_remaining" == "" ]; then
      batt_remaining="";
      if [ "$batt_percentage" != "100%" -o "$power_amp_raw" -lt "0" ]; then
        batt_remaining="$EN_SPACE$batt_percentage"
      fi

  elif [ "$(grep -o ' charging' <<< "$pmset_batt")" == " charging" ]; then
      if [[ $batt_percentage_raw > 85 ]]; then
          #batt_remaining="⁺$batt_remaining"$SUFFIX⚡"$batt_percentage"
          batt_remaining="⁺$batt_remaining"$SUFFIX" $EN_SPACE$batt_percentage"
      else
          batt_remaining="⁺$batt_remaining"$SUFFIX
      fi

  elif [ "$(grep -o 'finishing charge' <<< "$pmset_batt")" == " finishing charge" ]; then
      echo -n "finishing charge"
      #batt_remaining="⁺$batt_remaining"$SUFFIX⚡⚡"~$batt_percentage"
      batt_remaining="⁺$batt_remaining"$SUFFIX" $EN_SPACE~$batt_percentage"

  else
      #batt_remaining="$batt_remaining ($batt_time_at_full_charge)"
      batt_remaining="$batt_remaining"$SUFFIX
      #if [ "$batt_percentage" != "100%" ]; then
      #if [[ $batt_percentage_raw < 46 ]]; then
      if [[ $batt_percentage_raw < 31 ]]; then
        batt_remaining="$batt_remaining $EN_SPACE$batt_percentage"
      fi
  fi
  log3 "batt_remaining (formatted) = $batt_remaining"


# temporary
batt_remaining=$( clean <<< "$batt_remaining" )
log3 "clean => $batt_remaining"
if [ "$batt_remaining" == "" ] && $Is_discharging; then
      batt_remaining="+/-..."
fi
####batt_remaining="$EN_SPACE$batt_remaining"


#batt_remaining=""

#echo "$batt_remaining"; exit


## see above
# if [[ $batt_percentage_raw -lt 46 ]]; then
#   batt_remaining="$batt_remaining $EN_SPACE$batt_percentage"
# fi



#
#
#
  log "Wget"
##
## wget
##
##wgetoutput=` /usr/local/bin/wget --no-cache -O /dev/null http://www.google.com/                       2>&1`
##wgetoutput2=`/usr/local/bin/wget --no-cache -O /dev/null http://www.google.com/images/srpr/logo6w.png 2>&1`
##wgetoutput=` /usr/local/bin/wget --no-cache -O /dev/null http://www.bing.com/                         2>&1`
##wgetoutput2=`/usr/local/bin/wget --no-cache -O /dev/null http://www.bing.com/az/hprichbg/rb/DoubleArch_EN-US12966995277_1366x768.jpg  2>&1`
#wgetoutput=` /usr/local/bin/wget --no-cache -O /dev/null http://www.yahoo.com/ http://www.bing.com/                     2>&1`
#wgetoutput2=`/usr/local/bin/wget --no-cache -O /dev/null http://l.yimg.com/rz/l/ai_2014newyearseve_large_purple_x2.png http://www.bing.com/az/hprichbg/rb/DoubleArch_EN-US12966995277_1366x768.jpg 2>&1`

if false; then
  wgetoutput=` /usr/local/bin/wget --no-cache -O /dev/null https://www.google.com/                     2>&1`
  cannot=`echo "$wgetoutput" | grep -io 'failed' | head -n 1 | sed 's/failed/NO INTERNETS/' `
  #cannot="NO INTERNETS"
  wgetoutput=""
fi




log "IP address"

#ip_address=$( ifconfig | pcregrep -Mo '^en0:(.|\n)*?^en.:' | grep -o 'inet [0-9.]*' | cut -d ' ' -f 2 )
ip_address=$(      ifconfig en0 | grep inet | grep -v inet6 | grep -o 'inet [0-9.]*' | cut -d ' ' -f 2             )
ip_address_wired=$( ifconfig | /usr/local/bin/pcregrep -Mo '^en4:(.|\n)*'       | grep -o 'inet [0-9.]*' | cut -d ' ' -f 2 | head -n 1 )
ip_address=$(       sed 's/^192\.168//g' <<< "$ip_address"       )
ip_address_wired=$( sed 's/^192\.168//g' <<< "$ip_address_wired" )
#ip_address=$( which /usr/local/bin/pcregrep )

log2 $ip_address;

if  $Calm_network; then

    ip_address=""

fi



log "Internet"

speed=` echo  "$wgetoutput"  | grep -oi '[0-9., ]*.B/s' | tail -n 1`
speed2=`echo  "$wgetoutput2" | grep -oi '[0-9., ]*.B/s' | tail -n 1`
# Conversion is 1000 KB/s = 8 Mbps
if [ -n "$speed" ]
then
	# m = mean
	speed=`  echo $speed    | sed 's%KB/s%*8/1000.0%' | sed 's%MB/s%*8.0%' | bc -l`
	speed2=` echo $speed2   | sed 's%KB/s%*8/1000.0%' | sed 's%MB/s%*8.0%' | bc -l`
	speed_m=`echo "sqrt( $speed * $speed2 )" | bc -l`
	# clean trailing zeros
	#speed=` echo $speed  | sed -E 's%(([0-9]*)((\.[0-9]*[1-9])0*)?)%\2\4%'`
	#speed2=`echo $speed2 | sed -E 's%(([0-9]*)((\.[0-9]*[1-9])0*)?)%\2\4%'`
	#speed=` echo $speed +0.05 | bc -l | sed -E 's%(\.[1-9]).*%\1%' `
	#speed2=`echo $speed2+0.05 | bc -l | sed -E 's%(\.[1-9]).*%\1%' `
	speed=`  echo $speed   +0.05 | bc -l | sed -E 's%(\.[0-9]).*%\1%' `
	speed2=` echo $speed2  +0.05 | bc -l | sed -E 's%(\.[0-9]).*%\1%' `
	speed_m=`echo $speed_m +0.05 | bc -l | sed -E 's%(\.[0-9]).*%\1%' `
	# clean decimal
	speed=`  echo $speed   | sed 's%^\.%0.%'`
	speed2=` echo $speed2  | sed 's%^\.%0.%'`
	speed_m=`echo $speed_m | sed 's%^\.%0.%'`
	#speedstring='• '$speed2'-'$speed' Mbps'
	
	speed_mean=$speed_m
	if [ "$(echo "$speed_mean > 50" | bc -l)" == "1" ]; then
		speedstring=$(echo $speed_mean/8 |bc)' MB/s'
	else
		speedstring=$speed_mean' Mbps'
	fi
fi


##
## ping
##
# pingoutput=`ping google.com 2>&1 |head -n 2`
# cannot=`echo "$pingoutput"|head -n 1 | grep -io 'cannot' | sed 's/cannot/ • NO INTERNETS /' `
# pingtime=`echo "$pingoutput" | tail -n 1 | grep -o '[0-9.]* ms' | grep -o '^[0-9]*'`
# if [ -n "$pingtime" ]
# then
# 	pingtime=$pingtime'ms'
# fi

#
#
#
  log "CPU"
  
  ###
  ##  Notes
  ##  $ ps -A -O %cpu | (head; echo start; sleep 3; echo stop; sort -k 2 -h | tail -n 10 )
  ##
  
  if false; then

    ##top_output=`top -F -R -o cpu -l 1`
    # TODO reverse sorting in top, use head instead of 'tail'
    top_output=`top -F -R -o cpu -l 2`
    cpu=`echo "$top_output" | grep -oE 'CPU.*?user' | grep -oE '[0-9.]+' | tail -n 1`
    ctrl_m=`echo -en '\015'`
    hogger=`echo "$top_output" | tr $ctrl_m '\n' | grep -Eo '^.+PID' -A 1 | tail -n 1 | grep -oE '[0-9]+.+?:' | awk '{print $2}'`

    if [ "$(echo "$cpu > 1"| bc -l )" != "1" ]; then
      hogger="x"
    fi

  fi
  
    # added 6/13/2020
    list1="$( sleep 0.3 | ps -A -o %cpu )"
    list2="$( sleep 0.3 | ps -A -O %cpu )"
    cpu=$( echo "$list1" | awk '{s+=$1} END {print s "%"}' )
    processes="$( echo "$list2"  | grep -Ev '[0-9]* * 0.0 ' | sort -k 2 -h )"
    # | grep -oE '[^/ ]*($| )'
    hogger_cpu="$( echo "$processes" | tail -n 1 \
               | cut -c 7-13 \
               | xargs echo -n \
               | grep -oE '^[0-9]+' \
               )"
    hogger="$( echo "$processes" | tail -n 1 \
               | cut -c 34- \
               | grep -oE '^[^-]*' \
               | tail -n 1 \
               | grep -oE '[^/]*$' \
               | tail -n 1 \
               | tail -c 34 \
               )"
  
  



log  "Uptime string..."


#uptimestring=$( uptime | grep -o 'up.*day' | grep -oE '[0-9]+' )
uptimestring=$( uptime | grep -oE 'up.*day[^,]*,' | grep -oE '[0-9:]+' )
  log2  "$uptimestring"
uptimestring=$( echo -n $uptimestring | tr "\0\t\r\n " ","  )
  log2  "$uptimestring"





log  "Hour of week..."


hourofweek=$( ~/Documents/system/Git/status/get-week-hours.sh )
  log2  "$hourofweek"





log "Dates"

# See a listing at: http://en.wikipedia.org/wiki/List_of_tz_database_time_zones
nsw_date=" "`TZ=Australia/Sydney	  date +'%H:%M:%Z'`
art_date=`TZ=America/Buenos_Aires date +'%H:%M:%Z'`
mor_date=` TZ=WET                  date +'%H:%M:%Z'`
mor_dateX=`TZ=WET                  date +'%H:%M'`
utc_date=` TZ=UTC                  date +'%H:%M:%Z'`
utc_dateX=`TZ=UTC                  date +'%H:%M'`
#hkt_date=` TZ=Asia/Hong_Kong       date +'%H:%M'`
#hkt_dateX=`TZ=Asia/Hong_Kong       date +'%H:%M:%Z'`
##mor_date=`TZ=WET                  date +'%H:%M'`
##mor_date=`TZ=GMT-1                date +'%H:%M'`
##mor=`echo  المغرب‎ `
##mor='.ma'
if [ "$small_screen" = true ]; then
	dates=`TZ=UTC                  date +'%H'`
	utc_dateX=$dates
	dateTZ="U"
elif [ "$mor_dateX" == "$utc_dateX" ]; then
	#dates="$mor_date$mor"/UTC"$nsw_date"
	dates="$mor_dateX".M
	dateTZ="MU"
else
	#dates="$mor_date$mor$nsw_date $utc_date"
	dates="$mor_dateX".M" $utc_dateX".U
	dateTZ="U"
fi
#dates="$dates $hkt_dateX"
#override_dates="$nsw_date"

# fuck this shit
dates=""
dateTZ=""
utc_dateX=""



# (Arabic: المغرب‎ (al-Maġrib)

# Useful:
# echo q | top -F -R -o cpu | grep -Eo '^M.+[0-9]'
# echo q | top -F -R -o cpu | tr '^M' '\n' | grep -Eo '^.+[0-9]'

#if [ "$(echo "$cpu > 1"| bc -l )" == "1" ]
#then
#	echo -n "$hogger • $cpu""% • "
#fi



log "Done with calculations"

log "beginning rendering..."
#logDisplay "foobar"



if $small_screen; then
   SEPERATOR_DOT="$EN_SPACE"
   SEPERATOR_DOT_LEFT=""
fi



process_warnings=""
# #if [ $(ps aux | grep '[G]oogle Drive.app' | wc -l) -lt 1 ]; then
# #	process_warnings="$process_warnings [no google drive]"
# #fi
# #if [ true ] || 
# if [ $(ps aux | grep '[G]oogle Drive.app' | wc -l) -lt 1 ]; then
# 	process_warnings="$process_warnings no D"
# fi


# echo $(wc -c <<< "$clipboard")
clipboard=""


freespace=""
log "Freespace"
freespace=$( df -h /Users/allan | cut -c 28-33 | tail -n 1 )


###############################################################


if [ "$clipboard"                !=  ""    \
  -a "$clipboard"                !=  '``'  \
  -a $(wc -c <<< "$clipboard") -lt  39     \
   ]; then
#  -a $(wc -c <<< "asdfasdfasdf") -gt 20 \ 
# echo  -a $(wc -c <<< "$clipboard") -lt 5  \

	logDisplay clipboard
	echo -n "$clipboard"
	[ "$medium_screen" = false ] && echo  -n "$SEPERATOR_DOT"
fi

if [ "$process_warnings" != "" ]; then
	logDisplay process_warnings
	echo -n "$process_warnings"
	[ "$medium_screen" = false ] && echo  -n "$SEPERATOR_DOT"
fi
if [ "$screen_width" != "" ]; then
	logDisplay screen_width
	echo  -n "$screen_width"
	[ "$medium_screen" = false ] && echo  -n "$SEPERATOR_DOT" || echo -n ' '
fi



if [ "$cpu" != "" ]; then
	logDisplay cpu
	echo  -n "$cpu"
	echo  -n "$SEPERATOR_DOT"
fi


# ($art_date)
# TODO echo -n `top -l 1 | grep BBEdit | awk '{print $4}'`"$SEPERATOR_DOT" #BBEdit usage
echoed_network=1
if [ "$ssid" != "" ]; then
	echo  -n $ssid" "$pingtime$speedstring   #  2 spaces
fi
if [ "$ip_address" != "" ]; then
# if [ "$ip_address" != "" ] && [ "$medium_screen" = false ]; then
	logDisplay ip_address
	#echo  -n " "
	echo  -n "$ip_address"
	echo  -n "$SEPERATOR_DOT"
# fi
fi
if [ "$cannot" != "" ]; then
	logDisplay cannot
	echo  -n "$cannot "
	echo  -n "$SEPERATOR_DOT" # TODO prevent double dots
fi
if [ "$wired_string" != "" ]; then
	logDisplay wired_string
	echo  -n "$SEPERATOR_DOT"
	echo  -n "$wired_string"
	if [ "$ip_address" != "" ]; then
		logDisplay ip_address
		#echo  -n " "
		echo  -n "$ip_address_wired"
	fi
	echo  -n "$SEPERATOR_DOT"
fi


if [ "$freespace" != "" ]; then
	logDisplay freespace
	echo  -n "$freespace"
	echo  -n "$SEPERATOR_DOT"
	#$Calm_network && echo  -n " "
fi



if $Calm_network; then
    echo -n;
else



#if [ "$simple_work_msg" != "" ] -a 
simple_work_msg_len=$(  wc -c <<< "$simple_work_msg"  )
#echo -n "($simple_work_msg_len)"
if [[ $simple_work_msg_len  -gt  5 ]] && [[ $simple_work_msg_len  -lt  35 ]]; then
  logDisplay simple_work_msg
  echo -n "$EN_SPACE"
  echo -n "$simple_work_msg" 
fi



if [ "$work" != ""  ] && [[ "$work" != *"<>"* ]]; then
	logDisplay work
	work=$( echo "$work"   |  sed 's/ / /g' ) # UTF8 U+2005
	echo  -n "$work"
	echo  -n "$SEPERATOR_DOT"
fi


fi # !$Calm_network




#echo -n "$SEPERATOR_DOT"
#echo -n $processes
hour=`date +%H`
#value=$(echo "( $hour <= 19) + ($hour >= 9)" | bc -l)
value=0
if [ "$value" == "2" ]; then
	logDisplay dates
	echo  -n "$SEPERATOR_DOT"
	dates=$( echo "$dates"   |  sed 's/ / /g' ) # UTF8 U+2005
	echo  -n "$dates"
elif [ "$utc_dateX" != "" ]; then
	logDisplay utc_dateX
	if [ "$echoed_network" != "1" ] || [ "$medium_screen" = false ]; then
		echo  -n "$SEPERATOR_DOT"
	fi
	echo  -n "$utc_dateX.$dateTZ"
fi

#logDisplay override_dates
#echo  -n "$override_dates"

#echo  -n "$SEPERATOR_DOT"
#logDisplay cycles
#echo  -n $cycles #" cycles"

if [ "$linesChanged" != "" ]; then
	logDisplay linesChanged
	if [ "$small_screen" = false ]; then
		echo  -n "$SEPERATOR_DOT"
	fi
	linesChanged=$( echo "$linesChanged"   |  sed 's/ / /g' ) # UTF8 U+2005
	echo  -n $linesChanged
fi


if [ "$power" != "" ]; then
	logDisplay power
	#echo -n " "
	
	#if [ "$batt_remaining" == "" ] || [ "$small_screen" = false ]; then
	#	echo  -n "$SEPERATOR_DOT"
	#else
	#	echo -n " "
	#fi
	power=$( echo "$power"   |  sed 's/ / /g' ) # UTF8 U+2005
	echo  -n $power
	if  $Calm_network && test -n $NOIP && [ "$batt_remaining" != "" ]; then
		# We want this dot here to make things easier to read
		echo  -n "$SEPERATOR_DOT"
	fi
fi

# if [ "$batt_remaining" != "" ] || [ "$power" != "" ]; then
# 	logDisplay "batt_remaining || power"
#   echo -n "$EN_SPACE"
# fi


if [ "$batt_remaining" != "" ]; then
	logDisplay batt_remaining
	#if [ "$small_screen" = false ]; then
	#	echo  -n "$SEPERATOR_DOT"
	#else
	#	echo  -n " "
	#fi
	#######echo  -n "$EN_SPACE"
	#echo  -n "⌽"
	#echo  -n "–"
	batt_remaining=$( echo "$batt_remaining"   |  sed 's/ / /g' ) # UTF8 U+2005
	echo  -n $batt_remaining
	#echo  -n "h"
fi



logDisplay "uptime"
echo  -n "$SEPERATOR_DOT"
echo  -n "$uptimestring up"
#echo -n " up"


logDisplay "hourofweek"
echo  -n "$SEPERATOR_DOT"
$small_screen &&  echo  -n "$hourofweek""h"
$small_screen ||  echo  -n "$hourofweek""h of week"

#echo -n " up"




# if [ "$hogger" != "" ]; then
# 	logDisplay hogger
# 	echo  -n "$SEPERATOR_DOT"
# 	echo  -n "$hogger"
# 	echo  -n "$SEPERATOR_DOT"
# 	echo  -n "cpu:$cpu""%"
# fi




if [ "$cpu" != "" ]; then
	#logDisplay cpu
	#echo  -n "$SEPERATOR_DOT"
	#echo  -n "$cpu"
	if [ "$hogger" != "" ]; then
		logDisplay hogger
		echo  -n "$SEPERATOR_DOT"
		#echo -n " "
		echo  -n "$hogger_cpu%"
		echo  -n " "
		echo  -n "$hogger"
	fi
fi



$Calm_network && echo  -n "$SEPERATOR_DOT_LEFT"




#echo  -n "$SEPERATOR_DOT"
#echo -n "$( cat ~/status.sh--supplemental )"

#echo -n "$EN_SPACE •"
echo
log "done rendering"

#echo `date` $ssid $speed2 $speed >> speeds.log

