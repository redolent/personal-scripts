//
//  menuItem.m
//  mclock
//
//  Created by Palaniraja on 12/10/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "menuItem.h"


@implementation menuItem

- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code here.
        [[NSUserDefaults standardUserDefaults] registerDefaults:    
         [NSDictionary dictionaryWithObjectsAndKeys:     
          @"IST", kZoneString,     
          @"h:mm a", kDisplayFormatString,
          [NSNumber numberWithBool:1], kDisplayZonePrefix,
          nil]];
        
//        NSLog(@"Timezone abbrevation: %@", [NSTimeZone abbreviationDictionary]);

          showIpAddress = true;
    }
    
    return self;
}

- (void)dealloc
{
    [updateTimer invalidate];
    [formatter release];
    [updateTimer release];
    [zone release];
    [formatString release];
    [statusItem release];
    [prefix release];
//    [statusTitle release];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
}

#pragma mark -
-(void) updateLabel:(id)sender{
    
//    NSLog(@"Zone: %@ formatString: %@", zone, formatString);
//    statusTitle = [NSString stringWithFormat:@"%@%@", prefix, [formatter stringFromDate:[NSDate date]]];
//    NSLog(@"status title: %@", [NSString stringWithFormat:@"%@%@", prefix, [formatter stringFromDate:[NSDate date]]]);
    

//    [statusItem setTitle:[NSString stringWithFormat:@"%@%@", prefix, [formatter stringFromDate:[NSDate date]]]];

//    NSDictionary *titleAttributes = [NSDictionary
//                                     dictionaryWithObject:[NSColor lightGrayColor]
//                                     forKey:NSForegroundColorAttributeName];
//    NSFontDescriptor *fontDescriptor = [NSFontDescriptor
  //
    //                                    ];
    //NSFont *font = [NSFont fontWithName:@"Josefin Sans SemiBold" size:16.0];
    //NSFont *font = [NSFont fontWithName:@"Josefin Sans Bold" size:16.0];
    //NSFont *font = [NSFont fontWithName:@"Pontano Sans" size:15.0];
    
    // First choice:
    // NSFont *font = [NSFont fontWithName:@"Josefin Sans Bold" size:16.0];
    // Second choice:
    // NSFont *font = [NSFont fontWithName:@"American Typewriter Light" size:17.0];
    // Third choice:
    NSFont *font = [NSFont fontWithName:@"Lato Regular" size:13.0];

    
    //NSColor *myColor = [NSColor colorWithCalibratedHue:0.0 saturation:0.0 brightness:1.0 alpha:0.80];
    //NSColor *myColor = [NSColor colorWithCalibratedHue:0.0 saturation:1.0 brightness:1.0 alpha:0.80];
    NSColor *myColor = foregroundColor;
    NSDictionary *titleAttributes =
            [NSDictionary
             dictionaryWithObjectsAndKeys:
                      //[NSColor lightGrayColor],     NSForegroundColorAttributeName
                      myColor, NSForegroundColorAttributeName
                    , font,    NSFontAttributeName
                    //, [NSNumber numberWithFloat:5.0f], NSFontSizeAttribute
                    ,  nil
             
             
             ];
    //titleAttributes.
//
    //NSColor *myColor = [NSColor colorWithCalibratedHue:0.0 saturation:0.0 brightness:0.15 alpha:1.0];
    //NSDictionary *titleAttributes = [NSDictionary dictionaryWithObject:myColor forKey:NSForegroundColorAttributeName];
    //NSString* initString = [NSString stringWithFormat:@"%@%@", prefix, [formatter stringFromDate:[NSDate date]]];
    

    NSTask *task;
    task = [[NSTask alloc] init];
    [task setLaunchPath: @"/bin/bash"];
    
    NSArray *arguments;
    //arguments = [NSArray arrayWithObjects: @"foo", @"bar.txt", nil];
    arguments = [NSArray arrayWithObjects: @"/Users/allan/status.sh"
                                         , showIpAddress? @"" : @"--noip"
                                         , @""
                                         , nil];
    [task setArguments: arguments];
    
    NSPipe *pipe;
    pipe = [NSPipe pipe];
    [task setStandardOutput: pipe];
    
    NSFileHandle *file;
    file = [pipe fileHandleForReading];
    
    [task launch];
    
    NSData *data;
    data = [file readDataToEndOfFile];
    
    NSString *string;
    string = [[NSString alloc] initWithData: data encoding: NSUTF8StringEncoding];
    //NSLog (@"grep returned:\n%@", string);
    //string = @"hello";
    NSString *trimmedString = [string stringByTrimmingCharactersInSet:
                               [NSCharacterSet whitespaceAndNewlineCharacterSet]];

    // http://stackoverflow.com/questions/17266717/change-a-menu-bar-applications-font-size
    //NSAttributedString* whiteTitle = [[NSAttributedString alloc] initWithString:initString attributes:titleAttributes];
    NSAttributedString* whiteTitle = [[NSAttributedString alloc] initWithString:trimmedString attributes:titleAttributes];
    //[statusItem setTitle:initString];
    [statusItem setAttributedTitle:whiteTitle];

    //NSString *value = [string substringWithRange:NSMakeRange(6, 7)];
    //NSString *value = [string substringToIndex:4];
    //[statusItem setTitle:value];
    
    [string release];
    [task release];
    
    
}

- (void)awakeFromNib{
 
//    NSDictionary *titleAttributes = [NSDictionary dictionaryWithObject:[NSColor whiteColor] forKey:NSForegroundColorAttributeName];
//    NSAttributedString* whiteTitle = [[NSAttributedString alloc] initWithString:@"myTitle" attributes:titleAttributes];
    
    statusItem = [[[NSStatusBar systemStatusBar] statusItemWithLength:NSVariableStatusItemLength] retain];
//    [statusItem setAttributedTitle:whiteTitle];
    [statusItem setHighlightMode:YES];
    [statusItem setEnabled:YES];
    [statusItem setToolTip:@"Menu Clock (Mark)"];
    [statusItem setAction:@selector(updateLabel:)];
    [statusItem setTarget:self];
    [statusItem setMenu:statusMenu];
//    statusTitle = [[NSString alloc] init];
    
//    [whiteTitle release];
    
    zone          =  [[NSString stringWithFormat:@"CST"] retain]; //CST
    formatString  =  [[NSString stringWithFormat:@"h.mm a"] retain];  //HH:MM
    prefix        =  [[NSString stringWithFormat:@"%@ ", zone] retain];
    
    
  
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:formatString];
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:zone]]; 
    
    [self reload:nil];
    
    
    updateTimer =[[NSTimer scheduledTimerWithTimeInterval:120.0
                                     target:self
                                   selector:@selector(updateLabel:)
                                   userInfo:nil
                                    repeats:YES] retain];
    [updateTimer fire];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reload:) 
                                                 name:kPreferencesUpdated
                                               object:nil];
    
    
    
    
}

-(IBAction) showPreferences:(id)sender{
    

    if(!prefWindow){
        prefWindow = [[PreferenceWindowController alloc] init];  
    }
        
//    [prefWindow.window setLevel:NSScreenSaverWindowLevel + 1];
//    [prefWindow.window makeKeyAndOrderFront:nil];
    [NSApp activateIgnoringOtherApps:YES];
    
    [prefWindow showWindow:self];

    
}


-(IBAction) updateNow:(id)sender{
    
    [NSApp activateIgnoringOtherApps:YES];
    
    [self reload:nil];
    
    
}


-(IBAction) toggleShowIpAddress:(id)sender{
    
    [NSApp activateIgnoringOtherApps:YES];
    
    // hackey
    showIpAddress = !showIpAddress;

    [self reload:nil];

}


- (void) reload: (id)notification{
    
    
    if (foregroundColor) {
        [foregroundColor release];
    }
    if (zone) {
        [zone release];
    }
    if (formatString) {
        [formatString release];
    }
    
    if (prefix) {
        [prefix release];
    }
    
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    
    foregroundColor = [[ud objectForKey:kDisplayColor] retain];
    zone = [[ud objectForKey:kZoneString] retain];
    formatString = [[ud objectForKey:kDisplayFormatString] retain]; 

    //NSDictionary *titleAttributes = [NSDictionary dictionaryWithObject:[NSColor whiteColor] forKey:NSForegroundColorAttributeName];
    //NSAttributedString* whiteTitle = [[NSAttributedString alloc] initWithString:formatString attributes:titleAttributes];

    
    
    [formatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:zone]]; 
    [formatter setDateFormat:formatString];
    
    
    if ([ud boolForKey:kDisplayZonePrefix]) {
        prefix = [NSString stringWithFormat:@"%@ ", zone];
    }else{
        prefix = [NSString stringWithFormat:@""];
    }
    
//    NSLog(@"prefix: %@", prefix);
    [prefix retain];
    
    
    [self updateLabel:nil];
    
}

@end
